require('dotenv').config();
const express = require('express');

const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../../models/user');

// middlewares
const verifyToken = require('../../middlewares/verify-token');

router.post('/register', async (req, res) => {
  try {
    const { username, email, password } = req.body;

    if (!username || !email || !password) {
      res.status(400).send('All inputs are required: username, email, password');
      return;
    }

    const oldUser = await User.findOne({ username });
    if (oldUser) {
      res.status(400).send('User is already exist');
      return;
    }

    const encryptedPassword = await bcrypt.hash(password, 10);

    const potentialUser = await User.create({ username, email, password: encryptedPassword });

    res.status(201).send(potentialUser);
  } catch (error) {
    console.log(error);
    res.status(500).send();
  }
});

router.post('/login', async (req, res) => {
  try {
    const { username, password } = req.body;

    if (!username || !password) {
      res.status(400).send('All inputs are required: username, email, password');
      return;
    }

    const user = await User.findOne({ username }).select('+password');
    if (user && (await bcrypt.compare(password, user.password))) {
      const token = jwt.sign({ id: user.id }, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '1800s' });

      res.status(201).send({ token });
    }

    res.status(404).send('Email or password is wrong!');
  } catch (error) {
    console.log(error);
    res.status(500).send();
  }
});

router.get('/profile', verifyToken, async (req, res) => {
  try {
    const user = await User.findById(req.user.id).populate('articles').exec();

    res.status(201).send(user);
  } catch (error) {
    console.log(error);
    res.status(404).send();
  }
});

module.exports = router;
