const express = require('express');

const router = express.Router();
const Article = require('../../models/article');
const User = require('../../models/user');

// middlewares
const verifyToken = require('../../middlewares/verify-token');

router.get('/', async (req, res) => {
  try {
    const articles = await Article.find({});

    res.status(200).send(articles);
  } catch (error) {
    console.log(error);
    res.status(500).send();
  }
});

router.get('/:id', async (req, res) => {
  try {
    const article = await Article.findById(req.params.id);
    if (!article) {
      res.status(404).send();
      return;
    }

    res.status(200).send(article);
  } catch (error) {
    console.log(error);
    res.status(500).send();
  }
});

router.patch('/:id', verifyToken, async (req, res) => {
  try {
    const user = await User.findById(req.user.id);
    if (!user) { res.status(404).send('User is not exist!'); return; }

    const article = await Article.findById(req.params.id).populate('author').exec();
    if (article.author.id !== user.id) {
      res.status(403).send('You can not edit this article!');
      return;
    }
    await article.update({ title: req.body?.title, description: req.body?.description });

    res.status(201).send('Article is successfully edited');
  } catch (error) {
    console.log(error);
    res.status(404).send('Article is not found!');
  }
});

router.delete('/:id', verifyToken, async (req, res) => {
  try {
    const user = await User.findById(req.user.id);
    if (!user) { res.status(404).send('User is not exist!'); return; }

    const article = await Article.findById(req.params.id).populate('author').exec();
    if (article.author.id !== user.id) {
      res.status(403).send('You can not edit this article!');
      return;
    }

    await Article.deleteOne({ _id: req.params.id });

    res.status(201).send('Article is succesfully deleted.');
  } catch (error) {
    console.log(error);
    res.status(500).send();
  }
});

router.post('/', verifyToken, async (req, res) => {
  try {
    if (!req.body.title || !req.body.description) {
      res.status(400).send({ error: 'title, description and author fields are required' });
      return;
    }

    const user = await User.findById(req.user.id);
    if (!user) { res.status(404).send('User is not exist!'); return; }

    const article = await Article.create({
      title: req.body.title,
      description: req.body.description,
      author: user.id,
      coverImage: req.body.coverImage,
    });

    await user.articles.push(article);
    await user.save();

    res.status(201).send(article);
  } catch (error) {
    console.log(error);
    res.status(500).send();
  }
});

module.exports = router;
