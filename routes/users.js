var express = require('express');
var router = express.Router();

/* GET users listing. */

const users = [
  {
    name: "mkdir",
    role: "user"
  }
]

router.get('/', function (req, res, next) {
  res.send(users);
});

router.post('/', function (req, res, next) {
  users.push({ ...req.body, id: users.length + 1 })
  res.send(users);
});

router.patch('/:id', function (req, res, next) {
  console.log(req.params);
  const updatedUser = users.find(user => { user.id == req.params.id })
  res.send(updatedUser);
});

module.exports = router;
