const mongoose = require('mongoose');

const { Schema } = mongoose;

const articleSchema = new Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  coverImage: {
    type: String,
  },
}, {
  timestamps: true,
});

module.exports = mongoose.model('Article', articleSchema);
